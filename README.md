# README
# Cabify Challenge

##### Besides providing exceptional transportation services, Cabify also runs a physical store which sells (only) 3 products:

``` 
Code         | Name                |  Price
-------------------------------------------------
VOUCHER      | Cabify Voucher      |   5.00€
TSHIRT       | Cabify T-Shirt      |  20.00€
MUG          | Cafify Coffee Mug   |   7.50€
```

Various departments have insisted on the following discounts:

 * The marketing department believes in 2-for-1 promotions (buy two of the same product, get one free), and would like for there to be a 2-for-1 special on `VOUCHER` items.

 * The CFO insists that the best way to increase sales is with discounts on bulk purchases (buying x or more of a product, the price of that product is reduced), and demands that if you buy 3 or more `TSHIRT` items, the price per unit should be 19.00€.

Cabify's checkout process allows for items to be scanned in any order, and should return the total amount to be paid. The interface for the checkout process looks like this (ruby):

```ruby
co = Checkout.new(pricing_rules)
co.scan("VOUCHER")
co.scan("VOUCHER")
co.scan("TSHIRT")
price = co.total
```

Using ruby (>= 2.0), implement a checkout process that fulfills the requirements.

Examples:

    Items: VOUCHER, TSHIRT, MUG
    Total: 32.50€

    Items: VOUCHER, TSHIRT, VOUCHER
    Total: 25.00€

    Items: TSHIRT, TSHIRT, TSHIRT, VOUCHER, TSHIRT
    Total: 81.00€

    Items: VOUCHER, TSHIRT, VOUCHER, VOUCHER, MUG, TSHIRT, TSHIRT
    Total: 74.50€

**The code should:**
- Be written as production-ready code. You will write production code.
- Be easy to grow and easy to add new functionality.
- Have notes attached, explaining the solution and why certain things are included and others are left out.



### Installation instructions:


# Ruby version

ruby-2.5.1
rails-5.2.2

# Deployment instructions

* Uncompress program files or clone repository from git@gitlab.com:ggalia84/cabify-challenge.git in local folder.

* Configuration

Change config/database.yml file with database, user and local mysql password.


* Database creation

$ mysql -u root -p

mysql> CREATE DATABASE IF NOT EXISTS cabify_development;

mysql> CREATE DATABASE IF NOT EXISTS cabify_test;

mysql> GRANT ALL PRIVILEGES on cabify_development.* to 'root'@'localhost';

mysql> GRANT ALL PRIVILEGES on cabify_test.* to 'root'@'localhost';

mysql> FLUSH PRIVILEGES;

mysql> exit


* Create gemset with RVM:
    $ rvm use ruby-2.5.1@cabify --create


* Install Gems:
    $ gem install bundler
    $ bundle install


* Migrate and seed database:
    $ rails db:migrate
    $ rails db:seed


* Execute Rails server
    $ rails s


* Open project in web browser:
    http://localhost:3000


# How to run the test suite

$ rubocop
$ rspec

# Version Log

* 17 Dec, 2018 - Version: 0.0.1 - Start project.