# frozen_string_literal: true

Rails.application.routes.draw do
  root 'catalogs#index'

  get 'cart/index'
  resources :catalogs
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
