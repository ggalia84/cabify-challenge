# frozen_string_literal: true

class Catalog < ApplicationRecord
  belongs_to :offer, optional: true

  validates :code, :title, :price, presence: true
  validates :code, uniqueness: true
end
