# frozen_string_literal: true

class Offer < ApplicationRecord
  has_many :catalogs

  validates :name, :code, presence: true
  validates :code, uniqueness: true
end
