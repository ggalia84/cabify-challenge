# frozen_string_literal: true

class Checkout
  INITIAL_PRICE = 0
  INITIAL_ARTICLE_NUMBER = 0

  # Checkout object constructor.
  # param pricing_rules => ActiveRecord Collection model Offer.
  def initialize(pricing_rules)
    @articles = pricing_rules.blank? ? Catalog.includes(:offer).all : pricing_rules
    @price_total = INITIAL_PRICE
    @articles_number = get_articles_number_hash(@articles)
  end

  # Add new article number to @articles_number Hash.
  # param article_code => Unique Catalog code attribute.
  def scan(article_code)
    @articles_number[article_code] += 1 if @articles_number.key?(article_code)
  end

  # Get total articles price
  # return price_total => integer
  def total
    @articles_number.each { |article_code, number| @price_total += get_article_price(article_code, number) }
    @price_total
  end

  private

  # Get initial number of all catalog articles in database.
  # param articles => ActiveRecord Collection model Catalog.
  # return articles_hash => {'article_code' = article_number}.
  def get_articles_number_hash(articles)
    articles_hash = {}
    articles.each do |article|
      articles_hash[article.code] = INITIAL_ARTICLE_NUMBER
    end
    articles_hash
  end


  # Get price of article.
  # param article_code => String unique code of Catalog article.
  # param number => Integer number of added Catalog articles.
  # return price => Integer price of article.
  def get_article_price(article_code, number)
    price = 0
    article = @articles.find_by(code: article_code)
    if !number.blank? && !article.nil? && !article.price.blank? && !article.offer.nil?
      # TODO: Add new case function if add new Offer model / method
      price = case article.offer.code
              when '2_FOR_1'
                get_two_for_one_article_price(article, number)
              when 'X_OR_MORE'
                get_x_or_more_article_price(article, number)
              else
                get_article_price_without_offer(article, number) # Offer method not defined
              end
    elsif !number.blank? && !article.nil? && !article.price.blank? # Article without offer
      price = get_article_price_without_offer(article, number)
    end
    price
  end

  # TODO: Add new methods to calculate discount Offer price if require.

  # Get articles price with discount Offer '2_FOR_1'.
  # param article => ActiveRecord object model Catalog.
  # param number => Integer number of added Catalog articles.
  # return price => Integer price of article.
  def get_two_for_one_article_price(article, number)
    price = get_article_price_without_offer(article, number)
    if !number.blank? && validate_article_offer_2_for_1(article)
      price = if number.even?
                get_article_price_without_offer(article, number / 2)
              else
                article.price + get_article_price_without_offer(article, number / 2)
              end
    end
    price
  end

  # Get articles price with discount Offer 'X_OR_MORE'.
  # param article => ActiveRecord object model Catalog.
  # param number => Integer number of added Catalog articles.
  # return price => Integer price of article.
  def get_x_or_more_article_price(article, number)
    price = get_article_price_without_offer(article, number)
    price = (100 - article.discount_percentage).to_f / 100 * article.price * number if !number.blank? && validate_article_offer_x_or_more(article, number)
    price.to_i
  end

  # Get articles price without discount.
  # param article => ActiveRecord object model Catalog.
  # param number => Integer number of added Catalog articles.
  # return price => Integer price of article.
  def get_article_price_without_offer(article, number)
    article.price * number
  end

  # Validate articles attributes for Offer 2_FOR_1.
  # param article => ActiveRecord object model Catalog.
  # return boolean => true / false
  def validate_article_offer_2_for_1(article)
    !article.nil? && !article.price.blank? && !article.offer.nil? && [article.offer].include?(Offer.find_by(code: '2_FOR_1'))
  end

  # Validate articles attributes for Offer X_OR_MORE.
  # param article => ActiveRecord object model Catalog.
  # return boolean => true / false
  def validate_article_offer_x_or_more(article, number)
    !article.nil? && !article.price.blank? && !article.offer.nil? && [article.offer].include?(Offer.find_by(code: 'X_OR_MORE')) && !(article.discount_volume.blank? || article.discount_volume.negative? || article.discount_percentage.blank? || article.discount_percentage.negative? || article.discount_percentage > 100 || number < article.discount_volume)
  end
end
