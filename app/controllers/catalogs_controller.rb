# frozen_string_literal: true

class CatalogsController < ApplicationController

  # GET /catalogs
  def index
    @catalogs = Catalog.includes(:offer).all
  end

  private

  # Never trust parameters from the scary internet, only allow the white list through.
  def catalog_params
    params.require(:catalog).permit(:title, :description, :image, :price, :code, :discount_percentage, :discount_volume)
  end
end
