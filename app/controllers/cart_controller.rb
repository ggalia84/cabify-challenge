# frozen_string_literal: true

class CartController < ApplicationController
  SPLIT_PATTERN = ','

  # GET /cart/index
  def index
    article_codes_string = 'VOUCHER, TSHIRT, MUG'
    article_codes_array = article_codes_string.blank? ? nil : article_codes_string.gsub(/\s+/, '').split(SPLIT_PATTERN)

    if article_codes_array.blank?
      flash[:alert] = t('views.messages.alert.nothing_sended')
      redirect_to controller: 'catalogs', action: 'index'
    else
      @total_amount = get_total_amount(article_codes_array)
      @articles = get_articles_hash(article_codes_array)

      if @total_amount.nil? || @total_amount <= 0 || @articles.blank?
        flash[:alert] = t('views.messages.alert.something_wrong')
        redirect_to controller: 'catalogs', action: 'index'
      else
        flash[:notice] = t('views.messages.notice.cart_success')
      end
    end
  end

  private

  def get_total_amount(article_codes_array)
    pricing_rules = get_catalog_articles_from_array(article_codes_array)
    co = Checkout.new(pricing_rules)
    article_codes_array.each do |article_code|
      co.scan(article_code)
    end
    co.total
  end

  def get_articles_hash(article_codes_array)
    articles_hash = []
    catalog_articles = get_catalog_articles_from_array(article_codes_array)
    catalog_articles.each do |catalog_article|
      co = Checkout.new(catalog_articles)
      article_array = article_codes_array.find_all { |article| article == catalog_article.code }

      article_array.each do |article_code|
        co.scan(article_code)
      end
      tmp_hash = {}
      tmp_hash['image'] = catalog_article.image.blank? ? '' : catalog_article.image
      tmp_hash['title'] = catalog_article.title.blank? ? '' : catalog_article.title
      tmp_hash['price'] = catalog_article.price.blank? ? 0 : catalog_article.price
      tmp_hash['quantity'] = article_array.blank? ? 0 : article_array.count
      tmp_hash['subtotal'] = tmp_hash['price'] * tmp_hash['quantity']
      tmp_hash['offer'] = catalog_article.offer.nil? ? false : true
      tmp_hash['total'] = co.total
      articles_hash << tmp_hash
    end
    articles_hash
  end

  def get_catalog_articles_from_array(article_codes_array)
    Catalog.where(code: article_codes_array.uniq)
  end
end
