# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# OFFERS
offer_2_for_1 = Offer.create_with(name: '2 for 1', description: 'Get two articles, pay only one.').find_or_create_by!(code: '2_FOR_1')
offer_x_or_more = Offer.create_with(name: 'X or more', description: 'Get dicount for multiple articles.').find_or_create_by!(code: 'X_OR_MORE')

# CATALOGS
Catalog.create_with(title: 'Cabify Voucher', description: 'Gift amount may not be printed on Gift Cards. To check the gift amount, simply match the last 4 digits of the serial number on the back of the gift card to the one listed on the packing slip placed inside the box.', price: 500, image: 'voucher.png', offer: offer_2_for_1).find_or_create_by!(code: 'VOUCHER')

Catalog.create_with(title: 'Cabify T-Shirt', description: 'NON-SHRINK, QUICK-DRY: holds original shape and colour well even after many washes. Dries much faster than 100% cotton.', price: 2000, image: 'tshirt.png', discount_volume: 3, discount_percentage: 5, offer: offer_x_or_more).find_or_create_by!(code: 'TSHIRT')

Catalog.create_with(title: 'Cabify Coffe Mug', description: 'Ember keeps your coffee or tea at the perfect temperature from the first sip to the last.', price: 750, image: 'mug.png').find_or_create_by!(code: 'MUG')
