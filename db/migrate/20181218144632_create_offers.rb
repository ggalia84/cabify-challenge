# frozen_string_literal: true

class CreateOffers < ActiveRecord::Migration[5.2]
  def change
    create_table :offers do |t|
      t.string :name, null: false
      t.string :code, null: false
      t.string :description

      t.timestamps
    end
  end
end
