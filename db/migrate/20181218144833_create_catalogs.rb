# frozen_string_literal: true

class CreateCatalogs < ActiveRecord::Migration[5.2]
  def change
    create_table :catalogs do |t|
      t.string :title, null: false
      t.string :description
      t.string :image
      t.integer :price, default: 0
      t.string :code, null: false
      t.integer :discount_percentage, default: 0
      t.integer :discount_volume, default: 0
      t.references :offer, foreign_key: true

      t.timestamps
    end
  end
end
