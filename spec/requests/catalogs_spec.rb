# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Catalogs', type: :request do
  describe 'GET /catalogs' do
    it 'check HTTP status for index' do
      get catalogs_path
      expect(response).to have_http_status(200)
    end
  end
end
