# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Carts', type: :request do
  describe 'GET /cart/index' do
    it 'check HTTP status redirect for index' do
      get cart_index_path
      expect(response).to have_http_status(302)
    end
  end
end
