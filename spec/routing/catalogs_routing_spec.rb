# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CatalogsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/catalogs').to route_to('catalogs#index')
    end
  end
end
