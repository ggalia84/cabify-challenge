# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Offer, type: :model do
  describe 'when checking validations' do
    it 'accepts a valid offer' do
      offer = FactoryBot.build :offer
      expect(offer.valid?).to be true
      expect(offer.errors.empty?).to be true
    end
    it 'accepts a valid offer with name' do
      offer = FactoryBot.build :offer, name: 'New offer'
      expect(offer.valid?).to be true
      expect(offer.errors.empty?).to be true
    end
    it 'accepts a valid offer with code' do
      offer = FactoryBot.build :offer, code: 'NEW_CODE'
      expect(offer.valid?).to be true
      expect(offer.errors.empty?).to be true
    end
    it 'rejects offer without name' do
      offer = FactoryBot.build :offer, name: nil
      expect(offer.valid?).to be false
      expect(offer.errors['name'].present?).to be true
    end
    it 'rejects offer without code' do
      offer = FactoryBot.build :offer, code: nil
      expect(offer.valid?).to be false
      expect(offer.errors['code'].present?).to be true
    end
    it 'rejects offer if code already exists' do
      offer_1 = FactoryBot.build :offer
      offer_1.save
      offer_2 = FactoryBot.build :offer, code: offer_1.code
      offer_2.save
      expect(offer_2.valid?).to be false
      expect(offer_2.errors['code'].present?).to be true
    end
  end

  describe 'when checking associations offer' do
    it 'belongs to a offer' do
      expect(Offer.reflect_on_association(:catalogs).macro).to eq(:has_many)
    end
  end
end
