# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Catalog, type: :model do
  describe 'when checking validations' do
    it 'accepts a valid catalog' do
      catalog = FactoryBot.build :catalog
      expect(catalog.valid?).to be true
      expect(catalog.errors.empty?).to be true
    end
    it 'accepts a valid catalog with title' do
      catalog = FactoryBot.build :catalog, title: 'New catalog'
      expect(catalog.valid?).to be true
      expect(catalog.errors.empty?).to be true
    end
    it 'accepts a valid catalog with price' do
      catalog = FactoryBot.build :catalog, price: 4525
      expect(catalog.valid?).to be true
      expect(catalog.errors.empty?).to be true
    end
    it 'accepts a valid catalog with offer' do
      offer = FactoryBot.build :offer
      catalog = FactoryBot.build :catalog, offer: offer
      expect(catalog.valid?).to be true
      expect(catalog.errors.empty?).to be true
    end
    it 'rejects catalog without title' do
      catalog = FactoryBot.build :catalog, title: nil
      expect(catalog.valid?).to be false
      expect(catalog.errors['title'].present?).to be true
    end
    it 'rejects catalog without price' do
      catalog = FactoryBot.build :catalog, price: nil
      expect(catalog.valid?).to be false
      expect(catalog.errors['price'].present?).to be true
    end
    it 'rejects catalog without code' do
      catalog = FactoryBot.build :catalog, code: nil
      expect(catalog.valid?).to be false
      expect(catalog.errors['code'].present?).to be true
    end
    it 'rejects catalog if code already exists' do
      catalog_1 = FactoryBot.build :catalog
      catalog_1.save
      catalog_2 = FactoryBot.build :catalog, code: catalog_1.code
      catalog_2.save
      expect(catalog_2.valid?).to be false
      expect(catalog_2.errors['code'].present?).to be true
    end
  end

  describe 'when checking associations catalog' do
    it 'belongs to a offer' do
      expect(Catalog.reflect_on_association(:offer).macro).to eq(:belongs_to)
    end
  end
end
