# frozen_string_literal: true

FactoryBot.define do
  factory :offer do
    name { 'Name' }
    code { 'CODE' }
  end
end
