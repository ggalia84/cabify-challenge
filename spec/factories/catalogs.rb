# frozen_string_literal: true

FactoryBot.define do
  factory :catalog do
    title { 'Title' }
    code { 'CODE' }
    price { 750 }
  end
end
