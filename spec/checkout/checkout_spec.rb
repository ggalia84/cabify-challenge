# frozen_string_literal: true

require 'rails_helper'

def save_objects_database
  offer_2_for_1.save
  offer_x_or_more.save
  voucher.save
  shirt.save
  mug.save
end

RSpec.describe Checkout do
  # OFFERS
  let(:offer_2_for_1) { FactoryBot.create :offer, name: '2 for 1', description: 'Get two articles, pay only one.', code: '2_FOR_1' }
  let(:offer_x_or_more) { FactoryBot.create :offer, name: 'X or more', description: 'Get dicount for multiple articles.', code: 'X_OR_MORE' }
  # CATALOGS
  let(:voucher) { FactoryBot.create :catalog, title: 'Cabify Voucher', price: 500, image: 'voucher.png', offer: offer_2_for_1, code: 'VOUCHER' }
  let(:shirt) { FactoryBot.create :catalog, title: 'Cabify T-Shirt', price: 2000, image: 'tshirt.png', discount_volume: 3, discount_percentage: 5, offer: offer_x_or_more, code: 'TSHIRT' }
  let(:mug) { FactoryBot.create :catalog, title: 'Cabify Coffe Mug', price: 750, image: 'mug.png', code: 'MUG' }
  # CONSTANTS
  let(:catalog_codes_array) { %w(VOUCHER TSHIRT MUG) }
  let(:pricing_rules) { Catalog.includes(:offer).where(code: catalog_codes_array) }

  describe 'when checking validations' do
    it 'accepts a valid offer offer_2_for_1' do
      expect(offer_2_for_1.valid?).to be true
      expect(offer_2_for_1.errors.empty?).to be true
    end
    it 'accepts a valid offer offer_x_or_more' do
      expect(offer_x_or_more.valid?).to be true
      expect(offer_x_or_more.errors.empty?).to be true
    end
    it 'accepts a valid catalog voucher' do
      expect(voucher.valid?).to be true
      expect(voucher.errors.empty?).to be true
    end
    it 'accepts a valid catalog shirt' do
      expect(shirt.valid?).to be true
      expect(shirt.errors.empty?).to be true
    end
    it 'accepts a valid catalog mug' do
      expect(mug.valid?).to be true
      expect(mug.errors.empty?).to be true
    end
  end

  describe 'when execute class Checkout methods' do
    it 'initialize Checkout object class' do
      save_objects_database
      co = Checkout.new(pricing_rules)
      expect(co).not_to be_nil
      expect(co.class).to eq(Checkout)
      expect(co.total).to eq(0)
    end
    it 'check scan(article_code) and total() methods for [VOUCHER, TSHIRT, MUG]' do
      save_objects_database
      co = Checkout.new(pricing_rules)
      co.scan('VOUCHER')
      co.scan('TSHIRT')
      co.scan('MUG')
      expect(co.total).to eq(3250)
    end
    it 'check scan(article_code) and total() methods for [VOUCHER, TSHIRT, VOUCHER]' do
      save_objects_database
      co = Checkout.new(pricing_rules)
      co.scan('VOUCHER')
      co.scan('TSHIRT')
      co.scan('VOUCHER')
      expect(co.total).to eq(2500)
    end
    it 'check scan(article_code) and total() methods for [TSHIRT, TSHIRT, TSHIRT, VOUCHER, TSHIRT]' do
      save_objects_database
      co = Checkout.new(pricing_rules)
      co.scan('TSHIRT')
      co.scan('TSHIRT')
      co.scan('TSHIRT')
      co.scan('VOUCHER')
      co.scan('TSHIRT')
      expect(co.total).to eq(8100)
    end
    it 'check scan(article_code) and total() methods for [VOUCHER, TSHIRT, VOUCHER, VOUCHER, MUG, TSHIRT, TSHIRT]' do
      save_objects_database
      co = Checkout.new(pricing_rules)
      co.scan('VOUCHER')
      co.scan('TSHIRT')
      co.scan('VOUCHER')
      co.scan('VOUCHER')
      co.scan('MUG')
      co.scan('TSHIRT')
      co.scan('TSHIRT')
      expect(co.total).to eq(7450)
    end
  end
end